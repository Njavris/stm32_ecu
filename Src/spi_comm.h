#ifndef __SPI_COMM_H__
#define __SPI_COMM_H__

#include "types.h"

#define RX_SIZE	1
#define TX_SIZE	64
#define BUF_CNT	2

int init_spi(int spi_if);
void spi_data(uint8_t *data, int sz);

#endif
