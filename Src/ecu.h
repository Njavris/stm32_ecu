#ifndef __ECU_H__
#define __ECU_H__
#include "usart.h"
#include "adc.h"

int init_ecu(int uart_if, int spi_if);

void ecu_tick(void);

#endif
