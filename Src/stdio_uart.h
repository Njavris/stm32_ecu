#ifndef __STDIO_UART_H__
#define __STDIO_UART_H__
#include "types.h"
#include "usart.h"
#include "stdio.h"

int stdio_register_uart(int uart_if);
int _write(int f, char *buf, int len);
int _read (int file, char *ptr, int len);

#define ECU_DBG

#ifdef ECU_DBG
#define ECU_printf(...) printf(__VA_ARGS__)
#else
#define ECU_printf(...)
#endif

#endif
