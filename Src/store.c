#include "store.h"
#include "stdio.h"
#include "fatfs.h"
#include "string.h"
#include "fatfs_platform.h"
#include "stdio_uart.h"

static struct store_fs {
	FATFS sd_fatfs;
	char sd_path[4];
	int active;
	char fn[255];
} store;

int store_write(char *str, int len) {
	int ret;
	unsigned wlen;
	static FIL cap_file;
	if (!store.active) return 0;
	ret = f_open(&cap_file, store.fn, FA_OPEN_APPEND | FA_WRITE);
	if (ret == FR_NO_FILE)
		ret = f_open(&cap_file, store.fn, FA_CREATE_NEW | FA_WRITE);
	if (ret) return -1;
	ret = f_write(&cap_file, (uint8_t*)str, len, &wlen);
	ret = f_close(&cap_file);
	return wlen;
}
int find_file_name(void) {
	char *fn = store.fn;
	int res, filenum = 0;
	DIR dir;
	static FILINFO fno;

	res = f_opendir(&dir, "/");
	if (res)
		return res;

	while(1) {
		char *num, *pstfx, numstr[10];
		unsigned numlen;

		res = f_readdir(&dir, &fno);
		if (res || !fno.fname[0])
			break;
		ECU_printf("%s ", fno.fname);

		num = strstr(fno.fname, CAP_FILE_PRFX);
		if (!num)
			continue;

		num += strlen(CAP_FILE_PRFX);
		pstfx = strstr(num, CAP_FILE_PSTFX);
		if (!pstfx)
			continue;

		numlen = (uint32_t)pstfx - (uint32_t)num;
		if (!numlen)
			continue;

		memset(numstr, 0, sizeof(numstr));
		memcpy(numstr, num, numlen);
		filenum = atoi(numstr);
	}
	f_closedir(&dir);

	filenum ++;
	
	sprintf(fn, CAP_FILE_PRFX"%d"CAP_FILE_PSTFX, filenum);
	ECU_printf("\r\nNEW file name: %s\r\n", store.fn);
	return 0;
}

int init_store(void) {
//	DIR directory;
//	static uint8_t buffer[_MAX_SS];
	int ret = 0;

	memset(&store, 0, sizeof(struct store_fs));

	ret = BSP_PlatformIsDetected();
	if (ret != SD_PRESENT) {
		ECU_printf("SD card not detected\r\n");
		goto err;
	}
	ECU_printf("SD card detected\r\n");

	ret = f_mount(&store.sd_fatfs, (TCHAR const*)store.sd_path, 0);
#ifdef NEW_IF_NO_FS
	if (ret == FR_NO_FILESYSTEM) {
		ECU_printf("no FAT FS found creating a new one\r\n");
		ret = f_mkfs((TCHAR const*)store.sd_path, FM_FAT32, 0, buffer, sizeof(buffer));
		if (ret) {
			ECU_printf("failed to create new volume\r\n");
			goto err;
		}
	} else if (ret) {
#else
	if (ret) {
#endif
		ECU_printf("failed to mount SD card\r\n");
		goto err;
	}
	ECU_printf("Successfully mounted FAT file system\r\n");

//	ret = f_stat(CAP_DIR, NULL);
//	if (ret == FR_NO_FILE) {
//		ECU_printf("Couldn't find directory \"%s\", attemting to create it\r\n", CAP_DIR);
//		ret = f_mkdir((TCHAR const*)CAP_DIR);
//		if (ret) {
//			ECU_printf("Failed to create directory \"%s\"\r\n", CAP_DIR);
//			goto err;
//		}
//	}
//	if (!ret) {
//		ret = f_opendir(&directory, CAP_DIR);
//			if (ret != FR_OK) {
//				ECU_printf("Failed to change current directory\r\n");
//				goto err;
//			}
//	}
//	FILINFO fno;
//	while (1) {
//		ret = f_readdir(&directory, &fno);
//		if (ret || !fno.fname[0])
//			break;
//		ECU_printf("%s\r\n", fno.fname);
//	}
//
//	f_closedir(&directory);

	ret = find_file_name();
	if (ret) {
		ECU_printf("could not find a file to write\r\n");
		goto err;
	}

	store.active = 1;
	return 0;
err:
	ECU_printf("Err: %d, Proceeding without capturing\r\n", ret);
	return ret;
}

int deinit_store(void) {
	FATFS_UnLinkDriver(store.sd_path);
	return 0;
};
