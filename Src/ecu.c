#include "ecu.h"
#include "types.h"
#include "usart.h"
#include "stdio_uart.h"
#include "store.h"
#include "daq.h"
#include "string.h"
#include "stm32f4xx_hal.h"
#include "spi_comm.h"

static uint32_t jiffies;
static uint32_t last_jiffies;
static int lines_stored = 0;
char csv_line[256];
char *csv_off = csv_line;

enum dp_val_type {
	VAL_UINT16,
	VAL_UINT32,
	VAL_STR,
	VAL_MAX,
};

void append_dp_to_csv(enum dp_val_type type, void *val) {
	switch (type) {
	case VAL_UINT16 :
		sprintf(csv_off, "%u,", *(uint16_t*)val);
		csv_off = csv_line + strlen(csv_line);
		break;
	case VAL_UINT32 :
		sprintf(csv_off, "%lu,", *(uint32_t*)val);
		csv_off = csv_line + strlen(csv_line);
		break;
	case VAL_STR :
		sprintf(csv_off, "%s,", (char*)val);
		csv_off = csv_line + strlen(csv_line);
		break;
	default :
		return;
	}
}

void commit_to_storage_csv(void) {
	sprintf(csv_off - 1, "\r\n");
	store_write(csv_line, strlen(csv_line));
	csv_off = &csv_line[0];
	memset(csv_line, 0, sizeof(csv_line));
	lines_stored ++;
}

int init_ecu(int uart_if, int spi_if) {
	int i;
	stdio_register_uart(uart_if);
	ECU_printf("===================== ECU =====================\r\n");
	ECU_printf("codename: \"Whiny Alpaca\"\r\n");
	init_store();
	init_daq();
	init_spi(spi_if);
	jiffies = HAL_GetTick();

	append_dp_to_csv(VAL_STR, "ticks");
	for (i = 0; i < NUM_DAQ; i++) {
		char str[8];
		sprintf(str, "daq%d", i);
		append_dp_to_csv(VAL_STR, str);
	}
	commit_to_storage_csv();

	ECU_printf("Done\r\n");
	return 0;
}

void ecu_tick(void) {
	int i;
	uint16_t dp;
	uint16_t dp_arr[NUM_DAQ + 2] = {0};
	jiffies = HAL_GetTick();
	if ((jiffies - last_jiffies) >= 1000UL) {
		last_jiffies = jiffies;
		jiffies = HAL_GetTick();
		daq_dump_stats();
		ECU_printf("lines_stored: %d\r\n", lines_stored);
	}

	append_dp_to_csv(VAL_UINT32, &jiffies);
	*((uint32_t*)&dp_arr[0]) = jiffies;
	for (i = 0; i < NUM_DAQ; i++) {
		dp = daq_get_data_point(i);
		dp_arr[i + 2] = dp;
		append_dp_to_csv(VAL_UINT16, &dp);
	}
	spi_data((uint8_t*)dp_arr, (NUM_DAQ + 2) * sizeof(uint16_t));
	commit_to_storage_csv();

//	HAL_Delay(1);
}
