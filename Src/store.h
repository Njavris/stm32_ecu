#ifndef __DATA_CAPGER_H__
#define __DATA_CAPGER_H__
#include "types.h"

//#define NEW_IF_NO_FS
#define CAP_DIR	"DATA"
#define CAP_FILE_PRFX "DATA"
#define CAP_FILE_PSTFX ".CSV"

int init_store(void);
int deinit_store(void);
int store_write(char *str, int len);

#endif
