#ifndef __ADC_AQ_H__
#define __ADC_AQ_H__
#include "types.h"
#include "adc.h"

#define NUM_DAQ		3
#define ADC_BUF_LEN	1

int init_daq(void);
void daq_dump_stats(void);
uint16_t daq_get_data_point(int daq);

#endif
