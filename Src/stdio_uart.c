#include "stdio_uart.h"
#include "stdio.h"
#include "usart.h"

static UART_HandleTypeDef *uart = NULL;

int stdio_register_uart(int uart_if) {
	switch (uart_if) {
	case 3 : 
		uart = &huart3;
		break;
	default :
		return -1;
	}
	return 0;
}

int _write(int f, char *buf, int len) {
	if (!uart)
		return -1;
	if (HAL_UART_Transmit(uart, (uint8_t*)buf, len, HAL_MAX_DELAY) != HAL_OK)
		return 0;
	return len;
}

int _read (int file, char *ptr, int len) {
	if (!uart)
		return -1;
	HAL_UART_Receive(uart, (uint8_t*)ptr, 1, HAL_MAX_DELAY);
	HAL_UART_Transmit(uart, (uint8_t*)ptr, 1, HAL_MAX_DELAY);
	if (*ptr == '\r')
		HAL_UART_Transmit(uart, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
	return 1;
}

