#include "daq.h"
#include "adc.h"
#include "tim.h"
#include "string.h"
#include "stdio_uart.h"

struct daq_stats {
	unsigned irq_cnt;
};

struct daq_descr {
	ADC_HandleTypeDef *hadc;
	uint16_t buf[ADC_BUF_LEN];
	int curr_buf;
	int used_buf;
	struct daq_stats stats;
};

static struct daq_descr daqs[] = {
	{ .hadc = &hadc1, },
	{ .hadc = &hadc2, },
	{ .hadc = &hadc3, },
	{}
};

void daq_dump_stats(void) {
	int i;
	for (i = 0; i < NUM_DAQ; i++)
		ECU_printf("daq%d irqs:%d\r\n",
			i, daqs[i].stats.irq_cnt);
}

uint16_t daq_get_data_point(int daq) {
	return daqs[daq].buf[0];
}

void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef *hadc) {
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc) {
//ECU_printf("%d %d %d\r\n", daqs[0].buf[0], daqs[1].buf[0], daqs[2].buf[0]);
	int i;
	for (i = 0; i < NUM_DAQ; i++)
		if (daqs[i].hadc == hadc)
			break;
	daqs[i].stats.irq_cnt ++;
}

int init_daq(void) {
	int i, ret = 0;
	for (i = 0; i < NUM_DAQ; i++) {
		ret = HAL_ADC_Start_DMA(daqs[i].hadc,
			(uint32_t*)daqs[i].buf, ADC_BUF_LEN);
		if (ret != HAL_OK)
			ECU_printf("Failed DMA%d initialisation\r\n", i);
		else
			ECU_printf("DMA%d initialisation successful\r\n", i);
	}
	HAL_TIM_Base_Start_IT(&htim3);
	return 0;
}
