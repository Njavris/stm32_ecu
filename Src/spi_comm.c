#include "spi_comm.h"
#include "string.h"
#include "spi.h"
#include "stdio_uart.h"

enum spi_state {
	SPI_STATE_NONE = 0,
	SPI_STATE_RX,
	SPI_STATE_TX,
	SPI_STATE_MAX,
};

enum irq_type {
	SPI_IRQ_NONE = 0,
	SPI_IRQ_TX,
	SPI_IRQ_RX,
	SPI_IRQ_TXRX,
	SPI_IRQ_MAX,
};

static SPI_HandleTypeDef *spi;
static enum spi_state curr_state = SPI_STATE_NONE;

uint8_t rx_buf[RX_SIZE];
uint8_t tx_buf[TX_SIZE * BUF_CNT];
static int tx_sz = 0;
static int curr_buf = 0;

static uint8_t *get_buf(void) {
	return &tx_buf[TX_SIZE * curr_buf];
}

static void inc_buf(void) {
	curr_buf ++;
	curr_buf = BUF_CNT;
}

enum msg_type {
	SPI_MSG_NONE = 0,
	SPI_MSG_DP,
	SPI_MSG_DP_LEN,
	SPI_MSG_MAX,
};

static void rx_cmd_byte(void) {
	curr_state = SPI_STATE_RX;
	HAL_SPI_Receive_DMA(spi, rx_buf, 1);
}

static void cmd_decode(uint8_t cmd) {
	uint8_t *tmp;
	rx_buf[0] = 0;

	switch (cmd) {
	case SPI_MSG_DP :
		curr_state = SPI_STATE_TX;
		tmp = get_buf();
		HAL_SPI_Transmit_DMA(spi, tmp, tx_sz);
		inc_buf();
		break;
	case SPI_MSG_DP_LEN:
		curr_state = SPI_STATE_TX;
		tmp = &tx_sz;
		HAL_SPI_Transmit_DMA(spi, tmp, sizeof(tx_sz));
		break;
	default:
		rx_cmd_byte();
		break;
	}
}

static void msg_handle(enum irq_type irq) {
	switch (curr_state) {
	case SPI_STATE_RX :
		if (irq == SPI_IRQ_RX)
			cmd_decode(rx_buf[0]);
		break;
	case SPI_STATE_NONE :
	case SPI_STATE_TX :
		rx_cmd_byte();
	default :
		break;
	}
}

int init_spi(int spi_if) {
	switch(spi_if) {
	case 2 :
		spi = &hspi2;
		break;
	default :
		return -1;
	}

	msg_handle(SPI_IRQ_NONE);
	ECU_printf("Initialising spi%d\r\n", spi_if);
	return 0;
};

void spi_data(uint8_t *data, int sz) {
	uint8_t *tmp = get_buf();
	memcpy(tmp, data, sz);
	tx_sz = sz;
};

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi) {
	msg_handle(SPI_IRQ_TX);
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi) {
	msg_handle(SPI_IRQ_RX);
}

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi) {
}
